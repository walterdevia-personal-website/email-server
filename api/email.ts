import { NowRequest, NowResponse } from '@now/node'
import Mailgun, { messages, Mailgun as IMailgun } from 'mailgun-js'

interface Sender {
  name: string;
  subject: string;
  message: string;
}

export default async (request: NowRequest, response: NowResponse) => {
  setCors(response)

  if (isRequestMethod(request, 'options')) {
    return sendCors(response)
  }

  if (!isRequestMethod(request, 'post')) {
    return false
  }

  if (envVarsInvalid()) {
    console.log('Invalid env vars set.')
    return false;
  }

  const sender = getSender(request)

  if (!sender) {
    return response.json(false)
  }

  return sendEmail(sender)
    .then(result => response.json(result))
    .catch(() => response.json(false))
}

function isRequestMethod(request: NowRequest, method: string) {
  return typeof request.method === 'string' && request.method.toLowerCase() === method
}

function setCors(response: NowResponse) {
  response.setHeader('Access-Control-Allow-Origin', '*')
  response.setHeader('Access-Control-Allow-Methods', 'OPTIONS, POST')
  response.setHeader('Access-Control-Allow-Headers', '*')
}

function sendCors(response: NowResponse) {
  response.writeHead(200)
  response.end()
}

function envVarsInvalid(): boolean {
  return (
    typeof process.env.MAILGUN_API_KEY !== 'string' ||
    typeof process.env.MAILGUN_DOMAIN !== 'string' ||
    typeof process.env.ADDRESSEE !== 'string' ||
    typeof process.env.INTERNAL_SENDER_EMAIL !== 'string'
  )
}

function getSender(request: NowRequest): Sender | null {
  if (
    !request.body ||
    typeof request.body.name !== 'string' ||
    typeof request.body.subject !== 'string' ||
    typeof request.body.message !== 'string'
  ) {
    return null
  }

  return request.body
}

async function sendEmail(sender: Sender): Promise<boolean> {
  return new Promise<boolean>(resolve => {
    getMailgunInstance().messages().send(getMessageData(sender), (error, body) => {
      if (error) {
        console.error(`There was an error sending email. Sender: ${JSON.stringify(sender)}. Details below`, error)
        return resolve(false)
      }

      console.log(`New message sent. Details: ${JSON.stringify(body)}`)
      resolve(true)
    })
  })
}

function getMailgunInstance(): IMailgun {
  return new Mailgun({
    apiKey: process.env.MAILGUN_API_KEY!,
    domain: process.env.MAILGUN_DOMAIN!
  })
}

function getMessageData(sender: Sender): messages.SendData {
  return {
    from: process.env.INTERNAL_SENDER_EMAIL!,
    to: process.env.ADDRESSEE!,
    subject: 'Contact me email from personal website',
    text: `
    Sender name: ${sender.name}
    Sender subject: ${sender.subject}
    Message: ${sender.message}
    `
  }
}